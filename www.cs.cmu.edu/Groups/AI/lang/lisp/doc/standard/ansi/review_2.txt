Return-Path: <slug-errors@ai.sri.com>
Received: from CS.CMU.EDU by A.GP.CS.CMU.EDU id aa15901; 25 Jan 94 14:02:41 EST
Received: from Sunset.AI.SRI.COM by CS.CMU.EDU id aa15410;
          25 Jan 94 14:02:06 EST
Received: by Sunset.AI.SRI.COM (4.1/SMI-4.1)
	id AA27212 for mkant@cs.cmu.edu; Tue, 25 Jan 94 10:34:53 PST
Return-Path: <gls@Think.COM>
Received: from mail.think.com by Sunset.AI.SRI.COM (4.1/SMI-4.1)
	id AA27208 for /usr/lib/sendmail -oi -fslug-errors@ai.sri.com common-lisp-internal; Tue, 25 Jan 94 10:34:49 PST
Received: from Galileo.Think.COM by mail.think.com; Tue, 25 Jan 94 13:34:31 -0500
From: Guy Steele <gls@Think.COM>
Received: by galileo.think.com (4.1/Think-1.2)
	id AA06781; Tue, 25 Jan 94 13:34:31 EST
Date: Tue, 25 Jan 94 13:34:31 EST
Message-Id: <9401251834.AA06781@galileo.think.com>
To: x3j13@ai.sri.com, common-lisp@ai.sri.com, scheme@mc.lcs.mit.edu
Cc: gls@Think.COM
Subject: X3 announces second public review for Common Lisp


Below is the full text of the press release from X3.

It may be that Kent Pitman will arrange to make the latest draft
available on-line as he did for the first public review; if so,
details will follow in a later message.  (Note: the draft is about
1500 pages or so.  It's a lot to print.)

In any case, I remind you that any comments, to be officially
recognized, must be sent on paper to the two addresses listed
in the press release.  X3J13 would take it as a great favor if
any comments could also be sent electronically (to an address
that will be mentioned in the above-mentioned "later message").

--Guy Steele
---------------------------------------------------------------------------------
Accredited Standards Committee*			Date: January 11, 1994
X3, Information Processing Systems	     Project: 574-D
					    Reply to: Lynn Barra
						      202-626-5738
NEWS RELEASE					      75300.2665@compuserve.com
						  cc: G. Steele, X3J13 Chairman

	   X3 Announces the Second Public Review and Comment Period on
		  X3.226-199x, Programming Language Common Lisp

Washington, D.C. -- Accredited Standards Committee X3, Information Processing
Systems announces the two-month public review and comment period on X3.226-199x,
Programming Language Common Lisp.

The specification set forth in this document is designed to promote the
portability of Common Lisp programs among a variety of data processing systems.
It is a language specification aimed at an audience of implementors and
knowledgeable programmers.  It is neither a tutorial nor an implementation
guide.

The second public review is being announced due to substantive changes made to
the draft as a result of comments received during the first public review.

The comment period extends from February 4, 1994 through April 5, 1994.  Please
send all comments to: X3 Secretaiat, Attn.: Lynn Barra, 1250 Eye Street NW,
Suite 200, Washington, DC 20005-3922.  Send a copy to: American National
Standards Institute, Attn.: BSR Center, 11 West 42nd St. 13th Floor, New York,
NY 10036.

Purchase this standard from:

		       Global Engineering Documents, Inc.
				 2805 McGaw Ave.
				Irvine, CA  92714
			   1-800-854-7179 (within USA)
			   714-261-1455 (outside USA)

Single copy price:	$80.00
International price:	$104.00

   *Operating under the procedures of the Amrican National Standards Institute
X3 Secretariat, Computer and Business Equipment Manufacturers Association (CBEMA)
	      1250 Eye Street NW Suite 200 Washington DC 20005-3922
	  Telephone: (202)737-8888 (Press 1 twice)  FAX: (202)638-4922
---------------------------------------------------------------------------------
