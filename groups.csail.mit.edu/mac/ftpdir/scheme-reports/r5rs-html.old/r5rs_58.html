<HTML>
<HEAD>
<!-- This HTML file has been created by texi2html 1.54
     from r5rs.texi on 8 March 1998 -->

<TITLE>Revised(5) Report on the Algorithmic Language Scheme - Pairs and lists</TITLE>
<link href="r5rs_59.html" rel=Next>
<link href="r5rs_57.html" rel=Previous>
<link href="r5rs_toc.html" rel=ToC>

</HEAD>
<BODY>
<p>Go to the <A HREF="r5rs_1.html">first</A>, <A HREF="r5rs_57.html">previous</A>, <A HREF="r5rs_59.html">next</A>, <A HREF="r5rs_83.html">last</A> section, <A HREF="r5rs_toc.html">table of contents</A>.
<P><HR><P>


<H3><A NAME="SEC60" HREF="r5rs_toc.html#TOC60">6.3.2  Pairs and lists</A></H3>

<P>
A pair (sometimes called a dotted pair) is a record structure with two fields
called the car and cdr fields (for historical reasons).  Pairs are created by
the procedure cons.  The car and cdr fields are accessed by the procedures car
and cdr.  The car and cdr fields are assigned by the procedures set-car! and
set-cdr!.

</P>
<P>
Pairs are used primarily to represent lists.  A list can be defined recursively
as either the empty list or a pair whose cdr is a list.  More precisely, the
set of lists is defined as the smallest set X such that

</P>

<UL>

<LI>

The empty list is in X.

<LI>

f list is in X, then any pair whose cdr field contains list is also in X.

</UL>

<P>
The objects in the car fields of successive pairs of a list are the elements of
the list.  For example, a two-element list is a pair whose car is the first
element and whose cdr is a pair whose car is the second element and whose cdr
is the empty list.  The length of a list is the number of elements, which is
the same as the number of pairs.

</P>
<P>
The empty list is a special object of its own type (it is not a pair); it has
no elements and its length is zero.

</P>
<P>
Note: The above definitions imply that all lists have finite length and are
terminated by the empty list.

</P>
<P>
The most general notation (external representation) for Scheme pairs is the
"dotted" notation (c1 . c2) where c1 is the value of the car field and c2 is
the value of the cdr field.  For example (4 . 5) is a pair whose car is 4 and
whose cdr is 5.  Note that (4 . 5) is the external representation of a pair,
not an expression that evaluates to a pair.

</P>
<P>
A more streamlined notation can be used for lists: the elements of the list are
simply enclosed in parentheses and separated by spaces.  The empty list is
written () .  For example,

</P>

<PRE>
  (a b c d e)
</PRE>

<P>
and

</P>

<PRE>
  (a . (b . (c . (d . (e . ())))))
</PRE>

<P>
are equivalent notations for a list of symbols.

</P>
<P>
A chain of pairs not ending in the empty list is called an improper list.  Note
that an improper list is not a list.  The list and dotted notations can be
combined to represent improper lists:

</P>

<PRE>
  (a b c . d)
</PRE>

<P>
is equivalent to

</P>

<PRE>
  (a . (b . (c . d)))
</PRE>

<P>
Whether a given pair is a list depends upon what is stored in the cdr field.
When the set-cdr! procedure is used, an object can be a list one moment and not
the next:

</P>

<PRE>
  (define x (list 'a 'b 'c))
  (define y x)
  y                       =>  (a b c)
  (list? y)               =>  #t
  (set-cdr! x 4)          =>  unspecified
  x                       =>  (a . 4)
  (eqv? x y)              =>  #t
  y                       =>  (a . 4)
  (list? y)               =>  #f
  (set-cdr! x x)          =>  unspecified
  (list? x)               =>  #f
</PRE>

<P>
Within literal expressions and representations of objects read by the read
procedure, the forms '&#60;datum&#62;, `&#60;datum&#62;, ,&#60;datum&#62;, and ,@&#60;datum&#62; denote
two-element lists whose first elements are the symbols quote, quasiquote,
unquote, and unquote-splicing, respectively.  The second element in each case
is &#60;datum&#62;.  This convention is supported so that arbitrary Scheme programs may
be represented as lists.  That is, according to Scheme's grammar, every
&#60;expression&#62; is also a &#60;datum&#62; (see section 7.1.2).  Among other things, this
permits the use of the read procedure to parse Scheme programs.  See section
3.3.

</P>

<PRE>
[[procedure]] (pair? obj)
</PRE>

<P>
Pair? returns #t if obj is a pair, and otherwise returns #f.

</P>

<PRE>
  (pair? '(a . b))        =>  #t
  (pair? '(a b c))        =>  #t
  (pair? '())             =>  #f
  (pair? '#(a b))         =>  #f
</PRE>


<PRE>
[[procedure]] (cons obj1 obj2)
</PRE>

<P>
Returns a newly allocated pair whose car is obj1 and whose cdr is obj2.  The
pair is guaranteed to be different (in the sense of eqv?) from every existing
object.

</P>

<PRE>
  (cons 'a '())           =>  (a)
  (cons '(a) '(b c d))    =>  ((a) b c d)
  (cons "a" '(b c))       =>  ("a" b c)
  (cons 'a 3)             =>  (a . 3)
  (cons '(a b) 'c)        =>  ((a b) . c)
</PRE>


<PRE>
[[procedure]] (car pair)
</PRE>

<P>
Returns the contents of the car field of pair.  Note that it is an error to
take the car of the empty list.

</P>

<PRE>
  (car '(a b c))          =>  a
  (car '((a) b c d))      =>  (a)
  (car '(1 . 2))          =>  1
  (car '())               =>  error
</PRE>


<PRE>
[[procedure]] (cdr pair)
</PRE>

<P>
Returns the contents of the cdr field of pair.  Note that it is an error to
take the cdr of the empty list.

</P>

<PRE>
  (cdr '((a) b c d))      =>  (b c d)
  (cdr '(1 . 2))          =>  2
  (cdr '())               =>  error
</PRE>


<PRE>
[[procedure]] (set-car! pair obj)
</PRE>

<P>
Stores obj in the car field of pair.  The value returned by set-car! is
unspecified.

</P>

<PRE>
  (define (f) (list 'not-a-constant-list))
  (define (g) '(constant-list))
  (set-car! (f) 3)             =>  unspecified
  (set-car! (g) 3)             =>  error
</PRE>


<PRE>
[[procedure]] (set-cdr! pair obj)
</PRE>

<P>
Stores obj in the cdr field of pair.  The value returned by set-cdr! is
unspecified.

</P>

<PRE>
[[library procedure]] (caar pair)
[[library procedure]] (cadr pair)
    ...                   ...
[[library procedure]] (cdddar pair)
[[library procedure]] (cddddr pair)
</PRE>

<P>
These procedures are compositions of car and cdr, where for example caddr could
be defined by

</P>

<PRE>
  (define caddr (lambda (x) (car (cdr (cdr x))))).
</PRE>

<P>
Arbitrary compositions, up to four deep, are provided.  There are twenty-eight
of these procedures in all.

</P>

<PRE>
[[library procedure]] (null? obj)
</PRE>

<P>
Returns #t if obj is the empty list, otherwise returns #f.

</P>

<PRE>
[[library procedure]] (list? obj)
</PRE>

<P>
Returns #t if obj is a list, otherwise returns #f.  By definition, all lists
have finite length and are terminated by the empty list.

</P>

<PRE>
 (list? '(a b c))     =>  #t
 (list? '())          =>  #t
 (list? '(a . b))     =>  #f
 (let ((x (list 'a)))
   (set-cdr! x x)
   (list? x))         =>  #f
</PRE>


<PRE>
[[library procedure]] (list obj ...)
</PRE>

<P>
Returns a newly allocated list of its arguments.

</P>

<PRE>
  (list 'a (+ 3 4) 'c)            =>  (a 7 c)
  (list)                          =>  ()
</PRE>


<PRE>
[[library procedure]] (length list)
</PRE>

<P>
Returns the length of list.

</P>

<PRE>
  (length '(a b c))               =>  3
  (length '(a (b) (c d e)))       =>  3
  (length '())                    =>  0
</PRE>


<PRE>
[[library procedure]] (append list ...)
</PRE>

<P>
Returns a list consisting of the elements of the first list followed by the
elements of the other lists.

</P>

<PRE>
  (append '(x) '(y))              =>  (x y)
  (append '(a) '(b c d))          =>  (a b c d)
  (append '(a (b)) '((c)))        =>  (a (b) (c))
</PRE>

<P>
The resulting list is always newly allocated, except that it shares structure
with the last list argument.  The last argument may actually be any object; an
improper list results if the last argument is not a proper list.

</P>

<PRE>
  (append '(a b) '(c . d))        =>  (a b c . d)
  (append '() 'a)                 =>  a
</PRE>


<PRE>
[[library procedure]] (reverse list)
</PRE>

<P>
Returns a newly allocated list consisting of the elements of list in reverse
order.

</P>

<PRE>
  (reverse '(a b c))              =>  (c b a)
  (reverse '(a (b c) d (e (f))))  =>  ((e (f)) d (b c) a)
</PRE>


<PRE>
[[library procedure]] (list-tail list k)
</PRE>

<P>
Returns the sublist of list obtained by omitting the first k elements.  It is
an error if list has fewer than k elements.  List-tail could be defined by

</P>

<PRE>
  (define list-tail
    (lambda (x k)
      (if (zero? k)
          x
          (list-tail (cdr x) (- k 1)))))
</PRE>


<PRE>
[[library procedure]] (list-ref list k)
</PRE>

<P>
Returns the kth element of list.  (This is the same as the car of (list-tail
list k).)  It is an error if list has fewer than k elements.

</P>

<PRE>
  (list-ref '(a b c d) 2)                 =>  c
  (list-ref '(a b c d)
            (inexact-&#62;exact (round 1.8))) =>  c
</PRE>


<PRE>
[[library procedure]] (memq obj list)
[[library procedure]] (memv obj list)
[[library procedure]] (member obj list)
</PRE>

<P>
These procedures return the first sublist of list whose car is obj, where the
sublists of list are the non-empty lists returned by (list-tail list k) for k
less than the length of list.  If obj does not occur in list, then #f (not the
empty list) is returned.  Memq uses eq? to compare obj with the elements of
list, while memv uses eqv? and member uses equal?.

</P>

<PRE>
  (memq 'a '(a b c))              =>  (a b c)
  (memq 'b '(a b c))              =>  (b c)
  (memq 'a '(b c d))              =>  #f
  (memq (list 'a) '(b (a) c))     =>  #f
  (member (list 'a)
          '(b (a) c))             =>  ((a) c)
  (memq 101 '(100 101 102))       =>  unspecified
  (memv 101 '(100 101 102))       =>  (101 102)
</PRE>


<PRE>
[[library procedure]] (assq obj alist)
[[library procedure]] (assv obj alist)
[[library procedure]] (assoc obj alist)
</PRE>

<P>
Alist (for "association list") must be a list of pairs.  These procedures find
the first pair in alist whose car field is obj, and returns that pair.  If no
pair in alist has obj as its car, then #f (not the empty list) is returned.
Assq uses eq? to compare obj with the car fields of the pairs in alist, while
assv uses eqv? and assoc uses equal?.

</P>

<PRE>
  (define e '((a 1) (b 2) (c 3)))
  (assq 'a e)                =>  (a 1)
  (assq 'b e)                =>  (b 2)
  (assq 'd e)                =>  #f
  (assq (list 'a) '(((a)) ((b)) ((c))))
                             =>  #f
  (assoc (list 'a) '(((a)) ((b)) ((c))))
                             =>  ((a))
  (assq 5 '((2 3) (5 7) (11 13)))
                             =>  unspecified
  (assv 5 '((2 3) (5 7) (11 13)))
                             =>  (5 7)
</PRE>

<P>
Rationale: Although they are ordinarily used as predicates, memq, memv, member,
assq, assv, and assoc do not have question marks in their names because they
return useful values rather than just #t or #f.

</P>
<P><HR><P>
<p>Go to the <A HREF="r5rs_1.html">first</A>, <A HREF="r5rs_57.html">previous</A>, <A HREF="r5rs_59.html">next</A>, <A HREF="r5rs_83.html">last</A> section, <A HREF="r5rs_toc.html">table of contents</A>.
</BODY>
</HTML>
