<HTML>
<HEAD>
<!-- This HTML file has been created by texi2html 1.54
     from r5rs.texi on 8 March 1998 -->

<TITLE>Revised(5) Report on the Algorithmic Language Scheme - Control features</TITLE>
<link href="r5rs_64.html" rel=Next>
<link href="r5rs_62.html" rel=Previous>
<link href="r5rs_toc.html" rel=ToC>

</HEAD>
<BODY>
<p>Go to the <A HREF="r5rs_1.html">first</A>, <A HREF="r5rs_62.html">previous</A>, <A HREF="r5rs_64.html">next</A>, <A HREF="r5rs_83.html">last</A> section, <A HREF="r5rs_toc.html">table of contents</A>.
<P><HR><P>


<H2><A NAME="SEC65" HREF="r5rs_toc.html#TOC65">6.4  Control features</A></H2>

<P>
This chapter describes various primitive procedures which control the flow of
program execution in special ways.  The procedure? predicate is also described
here.

</P>

<PRE>
[[procedure]] (procedure? obj)
</PRE>

<P>
Returns #t if obj is a procedure, otherwise returns #f.

</P>

<PRE>
  (procedure? car)            =>  #t
  (procedure? 'car)           =>  #f
  (procedure? (lambda (x) (* x x)))
                              =>  #t
  (procedure? '(lambda (x) (* x x)))
                              =>  #f
  (call-with-current-continuation procedure?)
                              =>  #t
</PRE>


<PRE>
[[procedure]] (apply proc arg1 ... args)
</PRE>

<P>
Proc must be a procedure and args must be a list.  Calls proc with the elements
of the list (append (list arg1 ...) args) as the actual arguments.

</P>

<PRE>
  (apply + (list 3 4))              =>  7

  (define compose
    (lambda (f g)
      (lambda args
        (f (apply g args)))))

  ((compose sqrt *) 12 75)          =>  30
</PRE>


<PRE>
[[library procedure]] (map proc list1 list2 ...)
</PRE>

<P>
The lists must be lists, and proc must be a procedure taking as many arguments
as there are lists and returning a single value.  If more than one list is
given, then they must all be the same length.  Map applies proc element-wise to
the elements of the lists and returns a list of the results, in order.  The
dynamic order in which proc is applied to the elements of the lists is
unspecified.

</P>

<PRE>
  (map cadr '((a b) (d e) (g h)))   =>  (b e h)

  (map (lambda (n) (expt n n))
       '(1 2 3 4 5))                =>  (1 4 27 256 3125)

  (map + '(1 2 3) '(4 5 6))         =>  (5 7 9)

  (let ((count 0))
    (map (lambda (ignored)
           (set! count (+ count 1))
           count)
         '(a b)))                   =>  (1 2) or (2 1)
</PRE>


<PRE>
[[library procedure]] (for-each proc list1 list2 ...)
</PRE>

<P>
The arguments to for-each are like the arguments to map, but for-each calls
proc for its side effects rather than for its values.  Unlike map, for-each is
guaranteed to call proc on the elements of the lists in order from the first
element(s) to the last, and the value returned by for-each is unspecified.

</P>

<PRE>
  (let ((v (make-vector 5)))
    (for-each (lambda (i)
                (vector-set! v i (* i i)))
              '(0 1 2 3 4))
    v)                                =>  #(0 1 4 9 16)
</PRE>


<PRE>
[[library procedure]] (force promise)
</PRE>

<P>
Forces the value of promise (see delay, section 4.2.5).  If no value has been
computed for the promise, then a value is computed and returned.  The value of
the promise is cached (or "memoized") so that if it is forced a second time,
the previously computed value is returned.

</P>

<PRE>
  (force (delay (+ 1 2)))        =>  3
  (let ((p (delay (+ 1 2))))
    (list (force p) (force p)))  =>  (3 3)

  (define a-stream
    (letrec ((next
              (lambda (n)
                (cons n (delay (next (+ n 1)))))))
      (next 0)))
  (define head car)
  (define tail
    (lambda (stream) (force (cdr stream))))

  (head (tail (tail a-stream)))  =>  2
</PRE>

<P>
Force and delay are mainly intended for programs written in functional style.
The following examples should not be considered to illustrate good programming
style, but they illustrate the property that only one value is computed for a
promise, no matter how many times it is forced.

</P>

<PRE>
  (define count 0)
  (define p
    (delay (begin (set! count (+ count 1))
                  (if (&#62; count x)
                      count
                      (force p)))))
  (define x 5)
  p                     =>  a promise
  (force p)             =>  6
  p                     =>  a promise, still
  (begin (set! x 10)
         (force p))     =>  6
</PRE>

<P>
Here is a possible implementation of delay and force.  Promises are implemented
here as procedures of no arguments, and force simply calls its argument:

</P>

<PRE>
  (define force
    (lambda (object)
      (object)))
We define the expression
  (delay &#60;expression&#62;)
to have the same meaning as the procedure call
  (make-promise (lambda () &#60;expression&#62;))
as follows
  (define-syntax delay
    (syntax-rules ()
      ((delay expression)
       (make-promise (lambda () expression))))),
where make-promise is defined as follows:
  (define make-promise
    (lambda (proc)
      (let ((result-ready? #f)
            (result #f))
        (lambda ()
          (if result-ready?
              result
              (let ((x (proc)))
                (if result-ready?
                    result
                    (begin (set! result-ready? #t)
                           (set! result x)
                           result))))))))
</PRE>

<P>
Rationale: A promise may refer to its own value, as in the last example above.
Forcing such a promise may cause the promise to be forced a second time before
the value of the first force has been computed.  This complicates the
definition of make-promise.

</P>
<P>
Various extensions to this semantics of delay and force are supported in some
implementations:

</P>

<UL>

<LI>

Calling force on an object that is not a promise may simply return the object.

<LI>

It may be the case that there is no means by which a promise can be
operationally distinguished from its forced value.  That is, expressions like
the following may evaluate to either #t or to #f, depending on the
implementation:


<PRE>
    (eqv? (delay 1) 1)          =>  unspecified
    (pair? (delay (cons 1 2)))  =>  unspecified
</PRE>

<LI>

Some implementations may implement "implicit forcing," where the value of a
promise is forced by primitive procedures like cdr and +:


<PRE>
    (+ (delay (* 3 7)) 13)  =>  34
</PRE>

</UL>


<PRE>
[[procedure]] (call-with-current-continuation proc)
</PRE>

<P>
Proc must be a procedure of one argument. The procedure
call-with-current-continuation packages up the current continuation (see the
rationale below) as an "escape procedure" and passes it as an argument to proc.
The escape procedure is a Scheme procedure that, if it is later called, will
abandon whatever continuation is in effect at that later time and will instead
use the continuation that was in effect when the escape procedure was created.
Calling the escape procedure may cause the invocation of before and after
thunks installed using dynamic-wind.

</P>
<P>
The escape procedure accepts the same number of arguments as the continuation
to the original call to call-with-current-continuation.  Except for
continuations created by the call-with-values procedure, all continuations take
exactly one value.  The effect of passing no value or more than one value to
continuations that were not created by call-with-values is unspecified.

</P>
<P>
The escape procedure that is passed to proc has unlimited extent just like any
other procedure in Scheme.  It may be stored in variables or data structures
and may be called as many times as desired.

</P>
<P>
The following examples show only the most common ways in which
call-with-current-continuation is used.  If all real uses were as simple as
these examples, there would be no need for a procedure with the power of
call-with-current-continuation.

</P>

<PRE>
  (call-with-current-continuation
    (lambda (exit)
      (for-each (lambda (x)
                  (if (negative? x)
                      (exit x)))
                '(54 0 37 -3 245 19))
      #t))                        =>  -3

  (define list-length
    (lambda (obj)
      (call-with-current-continuation
        (lambda (return)
          (letrec ((r
                    (lambda (obj)
                      (cond ((null? obj) 0)
                            ((pair? obj)
                             (+ (r (cdr obj)) 1))
                            (else (return #f))))))
            (r obj))))))

  (list-length '(1 2 3 4))            =>  4

  (list-length '(a b . c))            =>  #f
</PRE>

<P>
Rationale:

</P>

<BLOCKQUOTE>

<P>
A common use of call-with-current-continuation is for structured, non-local
exits from loops or procedure bodies, but in fact
call-with-current-continuation is extremely useful for implementing a wide
variety of advanced control structures.

</P>
<P>
Whenever a Scheme expression is evaluated there is a continuation wanting the
result of the expression.  The continuation represents an entire (default)
future for the computation.  If the expression is evaluated at top level, for
example, then the continuation might take the result, print it on the screen,
prompt for the next input, evaluate it, and so on forever.  Most of the time
the continuation includes actions specified by user code, as in a continuation
that will take the result, multiply it by the value stored in a local variable,
add seven, and give the answer to the top level continuation to be printed.
Normally these ubiquitous continuations are hidden behind the scenes and
programmers do not think much about them.  On rare occasions, however, a
programmer may need to deal with continuations explicitly.
Call-with-current-continuation allows Scheme programmers to do that by creating
a procedure that acts just like the current continuation.

</P>
<P>
Most programming languages incorporate one or more special-purpose escape
constructs with names like exit, return, or even goto.  In 1965, however, Peter
Landin [16] invented a general purpose escape operator called the J-operator.
John Reynolds [24] described a simpler but equally powerful construct in 1972.
The catch special form described by Sussman and Steele in the 1975 report on
Scheme is exactly the same as Reynolds's construct, though its name came from a
less general construct in MacLisp.  Several Scheme implementors noticed that
the full power of the catch construct could be provided by a procedure instead
of by a special syntactic construct, and the name
call-with-current-continuation was coined in 1982.  This name is descriptive,
but opinions differ on the merits of such a long name, and some people use the
name call/cc instead.

</P>
</BLOCKQUOTE>


<PRE>
[[procedure]] (values obj ...)
</PRE>

<P>
Delivers all of its arguments to its continuation.  Except for continuations
created by the call-with-values procedure, all continuations take exactly one
value.  Values might be defined as follows:

</P>

<PRE>
  (define (values . things)
    (call-with-current-continuation
      (lambda (cont) (apply cont things))))
</PRE>


<PRE>
[[procedure]] (call-with-values producer consumer)
</PRE>

<P>
Calls its producer argument with no values and a continuation that, when passed
some values, calls the consumer procedure with those values as arguments.  The
continuation for the call to consumer is the continuation of the call to
call-with-values.

</P>

<PRE>
  (call-with-values (lambda () (values 4 5))
                    (lambda (a b) b))                =>  5

  (call-with-values * -)                             =>  -1
</PRE>


<PRE>
[[procedure]] (dynamic-wind before thunk after)
</PRE>

<P>
Calls thunk without arguments, returning the result(s) of this call.  Before
and after are called, also without arguments, as required by the following
rules (note that in the absence of calls to continuations captured using
call-with-current-continuation the three arguments are called once each, in
order).  Before is called whenever execution enters the dynamic extent of the
call to thunk and after is called whenever it exits that dynamic extent.  The
dynamic extent of a procedure call is the period between when the call is
initiated and when it returns.  In Scheme, because of
call-with-current-continuation, the dynamic extent of a call may not be a
single, connected time period.  It is defined as follows:

</P>

<UL>

<LI>

The dynamic extent is entered when execution of the body of the called
procedure begins.

<LI>

The dynamic extent is also entered when execution is not within the dynamic
extent and a continuation is invoked that was captured (using
call-with-current-continuation) during the dynamic extent.

<LI>

It is exited when the called procedure returns.

<LI>

It is also exited when execution is within the dynamic extent and a
continuation is invoked that was captured while not within the dynamic extent.

</UL>

<P>
If a second call to dynamic-wind occurs within the dynamic extent of the call
to thunk and then a continuation is invoked in such a way that the afters from
these two invocations of dynamic-wind are both to be called, then the after
associated with the second (inner) call to dynamic-wind is called first.

</P>
<P>
If a second call to dynamic-wind occurs within the dynamic extent of the call
to thunk and then a continuation is invoked in such a way that the befores from
these two invocations of dynamic-wind are both to be called, then the before
associated with the first (outer) call to dynamic-wind is called first.

</P>
<P>
If invoking a continuation requires calling the before from one call to
dynamic-wind and the after from another, then the after is called first.

</P>
<P>
The effect of using a captured continuation to enter or exit the dynamic extent
of a call to before or after is undefined.

</P>

<PRE>
  (let ((path '())
        (c #f))
    (let ((add (lambda (s)
                 (set! path (cons s path)))))
      (dynamic-wind
        (lambda () (add 'connect))
        (lambda ()
          (add (call-with-current-continuation
                 (lambda (c0)
                   (set! c c0)
                   'talk1))))
        (lambda () (add 'disconnect)))
      (if (&#60; (length path) 4)
          (c 'talk2)
          (reverse path))))
      => (connect talk1 disconnect
                   connect talk2 disconnect)
</PRE>

<P><HR><P>
<p>Go to the <A HREF="r5rs_1.html">first</A>, <A HREF="r5rs_62.html">previous</A>, <A HREF="r5rs_64.html">next</A>, <A HREF="r5rs_83.html">last</A> section, <A HREF="r5rs_toc.html">table of contents</A>.
</BODY>
</HTML>
