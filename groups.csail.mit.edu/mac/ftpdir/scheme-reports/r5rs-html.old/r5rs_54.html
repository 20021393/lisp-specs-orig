<HTML>
<HEAD>
<!-- This HTML file has been created by texi2html 1.54
     from r5rs.texi on 8 March 1998 -->

<TITLE>Revised(5) Report on the Algorithmic Language Scheme - Numerical operations</TITLE>
<link href="r5rs_55.html" rel=Next>
<link href="r5rs_53.html" rel=Previous>
<link href="r5rs_toc.html" rel=ToC>

</HEAD>
<BODY>
<p>Go to the <A HREF="r5rs_1.html">first</A>, <A HREF="r5rs_53.html">previous</A>, <A HREF="r5rs_55.html">next</A>, <A HREF="r5rs_83.html">last</A> section, <A HREF="r5rs_toc.html">table of contents</A>.
<P><HR><P>


<H3><A NAME="SEC56" HREF="r5rs_toc.html#TOC56">6.2.5  Numerical operations</A></H3>

<P>
The reader is referred to section 1.3.3 for a summary of the naming conventions
used to specify restrictions on the types of arguments to numerical routines.
The examples used in this section assume that any numerical constant written
using an exact notation is indeed represented as an exact number.  Some
examples also assume that certain numerical constants written using an inexact
notation can be represented without loss of accuracy; the inexact constants
were chosen so that this is likely to be true in implementations that use
flonums to represent inexact numbers.

</P>

<PRE>
[[procedure]] (number? obj)
[[procedure]] (complex? obj)
[[procedure]] (real? obj)
[[procedure]] (rational? obj)
[[procedure]] (integer? obj)
</PRE>

<P>
These numerical type predicates can be applied to any kind of argument,
including non-numbers.  They return #t if the object is of the named type, and
otherwise they return #f.  In general, if a type predicate is true of a number
then all higher type predicates are also true of that number.  Consequently, if
a type predicate is false of a number, then all lower type predicates are also
false of that number.

</P>
<P>
If z is an inexact complex number, then (real? z) is true if and only if (zero?
(imag-part z)) is true.  If x is an inexact real number, then (integer? x) is
true if and only if (= x (round x)).

</P>

<PRE>
  (complex? 3+4i)         =>  #t
  (complex? 3)            =>  #t
  (real? 3)               =>  #t
  (real? -2.5+0.0i)       =>  #t
  (real? #e1e10)          =>  #t
  (rational? 6/10)        =>  #t
  (rational? 6/3)         =>  #t
  (integer? 3+0i)         =>  #t
  (integer? 3.0)          =>  #t
  (integer? 8/4)          =>  #t
</PRE>

<P>
Note: The behavior of these type predicates on inexact numbers is unreliable,
since any inaccuracy may affect the result.

</P>
<P>
Note: In many implementations the rational? procedure will be the same as
real?, and the complex? procedure will be the same as number?, but unusual
implementations may be able to represent some irrational numbers exactly or may
extend the number system to support some kind of non-complex numbers.

</P>

<PRE>
[[procedure]] (exact? z)
[[procedure]] (inexact? z)
</PRE>

<P>
These numerical predicates provide tests for the exactness of a quantity.  For
any Scheme number, precisely one of these predicates is true.

</P>

<PRE>
[[procedure]] (= z1 z2 z3 ...)
[[procedure]] (&#60; x1 x2 x3 ...)
[[procedure]] (&#62; x1 x2 x3 ...)
[[procedure]] (&#60;= x1 x2 x3 ...)
[[procedure]] (&#62;= x1 x2 x3 ...)
</PRE>

<P>
These procedures return #t if their arguments are (respectively): equal,
monotonically increasing, monotonically decreasing, monotonically
nondecreasing, or monotonically nonincreasing.

</P>
<P>
These predicates are required to be transitive.

</P>
<P>
Note: The traditional implementations of these predicates in Lisp-like
languages are not transitive.

</P>
<P>
Note: While it is not an error to compare inexact numbers using these
predicates, the results may be unreliable because a small inaccuracy may affect
the result; this is especially true of = and zero?.  When in doubt, consult a
numerical analyst.

</P>

<PRE>
[[library procedure]] (zero? z)
[[library procedure]] (positive? x)
[[library procedure]] (negative? x)
[[library procedure]] (odd? n)
[[library procedure]] (even? n)
</PRE>

<P>
These numerical predicates test a number for a particular property, returning
#t or #f.  See note above.

</P>

<PRE>
[[library procedure]] (max x1 x2 ...)
[[library procedure]] (min x1 x2 ...)
</PRE>

<P>
These procedures return the maximum or minimum of their arguments.

</P>

<PRE>
  (max 3 4)              =>  4    ; exact
  (max 3.9 4)            =>  4.0  ; inexact
</PRE>

<P>
Note: If any argument is inexact, then the result will also be inexact (unless
the procedure can prove that the inaccuracy is not large enough to affect the
result, which is possible only in unusual implementations).  If min or max is
used to compare numbers of mixed exactness, and the numerical value of the
result cannot be represented as an inexact number without loss of accuracy,
then the procedure may report a violation of an implementation restriction.

</P>

<PRE>
[[procedure]] (+ z1 ...)
[[procedure]] (* z1 ...)
</PRE>

<P>
These procedures return the sum or product of their arguments.

</P>

<PRE>
  (+ 3 4)                 =>  7
  (+ 3)                   =>  3
  (+)                     =>  0
  (* 4)                   =>  4
  (*)                     =>  1
</PRE>


<PRE>
[[procedure]] (- z1 z2)
[[procedure]] (- z)
[[optional procedure]] (- z1 z2 ...)
[[procedure]] (/ z1 z2)
[[procedure]] (/ z)
[[optional procedure]] (/ z1 z2 ...)
</PRE>

<P>
With two or more arguments, these procedures return the difference or quotient
of their arguments, associating to the left.  With one argument, however, they
return the additive or multiplicative inverse of their argument.

</P>

<PRE>
  (- 3 4)                 =>  -1
  (- 3 4 5)               =>  -6
  (- 3)                   =>  -3
  (/ 3 4 5)               =>  3/20
  (/ 3)                   =>  1/3
</PRE>


<PRE>
[[library procedure]] (abs x)
</PRE>

<P>
Abs returns the absolute value of its argument.

</P>

<PRE>
  (abs -7)                =>  7
</PRE>


<PRE>
[[procedure]] (quotient n1 n2)
[[procedure]] (remainder n1 n2)
[[procedure]] (modulo n1 n2)
</PRE>

<P>
These procedures implement number-theoretic (integer) division.  n2 should be
non-zero.  All three procedures return integers.  If n1/n2 is an integer:

</P>

<PRE>
  (quotient n1 n2)   => n1/n2
  (remainder n1 n2)  => 0
  (modulo n1 n2)     => 0
</PRE>

<P>
If n1/n2 is not an integer:

</P>

<PRE>
  (quotient n1 n2)   => n_q
  (remainder n1 n2)  => n_r
  (modulo n1 n2)     => n_m
</PRE>

<P>
where n_q is n1/n2 rounded towards zero, 0 &#60; |nr| &#60; |n2|, 0 &#60; |nm| &#60;
|n2|, n_r and n_m differ from n1 by a multiple of n2, n_r has the same sign as
n1, and n_m has the same sign as n2.

</P>
<P>
From this we can conclude that for integers n1 and n2 with n2 not equal to 0,

</P>

<PRE>
  (= n1 (+ (* n2 (quotient n1 n2))
        (remainder n1 n2)))   =>  #t
</PRE>

<P>
provided all numbers involved in that computation are exact.

</P>

<PRE>
  (modulo 13 4)           =>  1
  (remainder 13 4)        =>  1

  (modulo -13 4)          =>  3
  (remainder -13 4)       =>  -1

  (modulo 13 -4)          =>  -3
  (remainder 13 -4)       =>  1

  (modulo -13 -4)         =>  -1
  (remainder -13 -4)      =>  -1

  (remainder -13 -4.0)    =>  -1.0  ; inexact
</PRE>


<PRE>
[[library procedure]] (gcd n1 ...)
[[library procedure]] (lcm n1 ...)
</PRE>

<P>
These procedures return the greatest common divisor or least common multiple of
their arguments.  The result is always non-negative.

</P>

<PRE>
  (gcd 32 -36)            =>  4
  (gcd)                   =>  0
  (lcm 32 -36)            =>  288
  (lcm 32.0 -36)          =>  288.0  ; inexact
  (lcm)                   =>  1
</PRE>


<PRE>
[[procedure]] (numerator q)
[[procedure]] (denominator q)
</PRE>

<P>
These procedures return the numerator or denominator of their argument; the
result is computed as if the argument was represented as a fraction in lowest
terms.  The denominator is always positive.  The denominator of 0 is defined to
be 1.

</P>

<PRE>
  (numerator (/ 6 4))  =>  3
  (denominator (/ 6 4))  =>  2
  (denominator
    (exact-&#62;inexact (/ 6 4))) => 2.0
</PRE>


<PRE>
[[procedure]] (floor x)
[[procedure]] (ceiling x)
[[procedure]] (truncate x)
[[procedure]] (round x)
</PRE>

<P>
These procedures return integers.  Floor returns the largest integer not larger
than x.  Ceiling returns the smallest integer not smaller than x.  Truncate
returns the integer closest to x whose absolute value is not larger than the
absolute value of x.  Round returns the closest integer to x, rounding to even
when x is halfway between two integers.

</P>
<P>
Rationale: Round rounds to even for consistency with the default rounding mode
specified by the IEEE floating point standard.

</P>
<P>
Note: If the argument to one of these procedures is inexact, then the result
will also be inexact.  If an exact value is needed, the result should be passed
to the inexact-&#62;exact procedure.

</P>

<PRE>
  (floor -4.3)          =>  -5.0
  (ceiling -4.3)        =>  -4.0
  (truncate -4.3)       =>  -4.0
  (round -4.3)          =>  -4.0

  (floor 3.5)           =>  3.0
  (ceiling 3.5)         =>  4.0
  (truncate 3.5)        =>  3.0
  (round 3.5)           =>  4.0  ; inexact

  (round 7/2)           =>  4    ; exact
  (round 7)             =>  7
</PRE>


<PRE>
[[library procedure]] (rationalize x y)
</PRE>

<P>
Rationalize returns the simplest rational number differing from x by no more
than y.  A rational number r1 is simpler than another rational number r2 if
r1 = p1/q1 and r2 = p2/q2 (in lowest terms) and |p1| &#60;= |p2| and
|q1| &#60;= |q2|.  Thus 3/5 is simpler than 4/7.  Although not all rationals
are comparable in this ordering (consider 2/7 and 3/5) any interval contains a
rational number that is simpler than every other rational number in that
interval (the simpler 2/5 lies between 2/7 and 3/5).  Note that 0 = 0/1 is the
simplest rational of all.

</P>

<PRE>
  (rationalize
    (inexact-&#62;exact .3) 1/10)  => 1/3    ; exact
  (rationalize .3 1/10)        => #i1/3  ; inexact
</PRE>


<PRE>
[[procedure]] (exp z)
[[procedure]] (log z)
[[procedure]] (sin z)
[[procedure]] (cos z)
[[procedure]] (tan z)
[[procedure]] (asin z)
[[procedure]] (acos z)
[[procedure]] (atan z)
[[procedure]] (atan y x)
</PRE>

<P>
These procedures are part of every implementation that supports general real
numbers; they compute the usual transcendental functions.  Log computes the
natural logarithm of z (not the base ten logarithm).  Asin, acos, and atan
compute arcsine (sin^-1), arccosine (cos^-1), and arctangent (tan^-1),
respectively.  The two-argument variant of atan computes (angle
(make-rectangular x y)) (see below), even in implementations that don't support
general complex numbers.

</P>
<P>
In general, the mathematical functions log, arcsine, arccosine, and arctangent
are multiply defined.  The value of log(z) is defined to be the one whose
imaginary part lies in the range from -pi (exclusive) to pi (inclusive).
log(0) is undefined.  With log defined this way, the values of sin^-1(z),
cos^-1(z), and tan^-1(z) are according to the following formulae:

</P>

<PRE>
  sin^-1(z) = -i*log(i*z + sqrt(1 - z^2))
  cos^-1(z) = pi/2 - sin^-1(z)
  tan^-1(z) = (log(1 + i*z) - log(1 - i*z)) / (2*i)
</PRE>

<P>
The above specification follows [27], which in turn cites [19]; refer to these
sources for more detailed discussion of branch cuts, boundary conditions, and
implementation of these functions.  When it is possible these procedures
produce a real result from a real argument.

</P>

<PRE>
[[procedure]] (sqrt z)
</PRE>

<P>
Returns the principal square root of z.  The result will have either positive
real part, or zero real part and non-negative imaginary part.

</P>

<PRE>
[[procedure]] (expt z1 z2)
</PRE>

<P>
Returns z1 raised to the power z2.  For z1 =/= 0

</P>

<PRE>
  z1^z2 = e^(z2*log(z1))
</PRE>

<P>
0^z is 1 if z = 0 and 0 otherwise.

</P>

<PRE>
[[procedure]] (make-rectangular x1 x2)
[[procedure]] (make-polar x3 x)
[[procedure]] (real-part z)
[[procedure]] (imag-part z)
[[procedure]] (magnitude z)
[[procedure]] (angle z)
</PRE>

<P>
These procedures are part of every implementation that supports general complex
numbers.  Suppose x1, x2, x3, and x4 are real numbers and z is a complex number
such that

</P>

<PRE>
  z = x1 + x2*i = x3 * e^(i*x4)
</PRE>

<P>
Then

</P>

<PRE>
  (make-rectangular x1 x2) => z
  (make-polar x3 x4)     => z
  (real-part z)                  => x1
  (imag-part z)                  => x2
  (magnitude z)                  => |x3|
  (angle z)                      => x_angle
</PRE>

<P>
where -pi &#60; x_angle &#60;= pi with x_angle = x4 + 2*pi*n for some integer n.

</P>
<P>
Rationale: Magnitude is the same as abs for a real argument, but abs must be
present in all implementations, whereas magnitude need only be present in
implementations that support general complex numbers.

</P>

<PRE>
[[procedure]] (exact-&#62;inexact z)
[[procedure]] (inexact-&#62;exact z)
</PRE>

<P>
Exact-&#62;inexact returns an inexact representation of z.  The value returned is
the inexact number that is numerically closest to the argument.  If an exact
argument has no reasonably close inexact equivalent, then a violation of an
implementation restriction may be reported.

</P>
<P>
Inexact-&#62;exact returns an exact representation of z.  The value returned is the
exact number that is numerically closest to the argument.  If an inexact
argument has no reasonably close exact equivalent, then a violation of an
implementation restriction may be reported.

</P>
<P>
These procedures implement the natural one-to-one correspondence between exact
and inexact integers throughout an implementation-dependent range.  See section
6.2.3.

</P>
<P><HR><P>
<p>Go to the <A HREF="r5rs_1.html">first</A>, <A HREF="r5rs_53.html">previous</A>, <A HREF="r5rs_55.html">next</A>, <A HREF="r5rs_83.html">last</A> section, <A HREF="r5rs_toc.html">table of contents</A>.
</BODY>
</HTML>
